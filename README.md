
 - first run `docker-compose build`
 - then `docker-compose up -d`
 - finally `docker-compose logs -f` to see the console

 In Postman you can `POST localhost:80/users` with a json `{"name":"pikachu", "email":"pikachu@comet.co"}`
 Then `GET localhost:80` to retrieve all users entries

the container will automatically restart when a file change, thanks to `RUN npm install -g nodemon` in `Dockerfile` and `command: nodemon app.js` in `docker-compose.yml`