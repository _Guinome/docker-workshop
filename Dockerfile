FROM node:8
RUN mkdir /docker-workshop
RUN npm install -g nodemon
COPY ./ /docker-workshop
WORKDIR /docker-workshop
RUN npm i
# RUN node ./db/init.js
EXPOSE 80
CMD ["npm", "start"]