const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser'); // Charge le middleware de gestion des paramètres
const urlencodedParser = bodyParser.urlencoded({ extended: false });
const { Client } = require('pg');
const config = {
    // host: 'localhost:5432/docker-workshop'
    // user: 'postgres', // default process.env.PGUSER || process.env.USER
    // password: 'postgres', //default process.env.PGPASSWORD
    // database?: string, // default process.env.PGDATABASE || process.env.USER
    // port: 5432, // default process.env.PGPORT
    connectionString: 'postgres://postgres:password@db:5432/docker-workshop' // e.g. postgres://user:password@host:5432/database
    // ssl?: any, // passed directly to node.TLSSocket
    // types?: any, // custom type parsers
    // statement_timeout: number, // number of milliseconds before a query will time out default is no timeout
}
const client = new Client(config);
client.connect();


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


/* GET all entities. */
app.get('/', function (req, res, next) {
  const query = 'SELECT * FROM users';
  client.query(query)
    .then(res2 => res.status(200).send(res2.rows))
    .catch(e => {
      console.log(client);
      res.status(400).send(e);
    });
})
app.post('/users', urlencodedParser, function (req, res) {
  const query = 'INSERT INTO users(name, email) VALUES($1, $2)';
  const values = [req.body.name, req.body.email];
  client.query(query, values)
    .then(res2 => res.redirect('/'))
    .catch(e => console.error(e.stack));
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
