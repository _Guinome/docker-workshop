const { Client } = require('pg');
const config = {
    // host: 'localhost:5432/docker-workshop'
    // user: 'postgres', // default process.env.PGUSER || process.env.USER
    // password: 'postgres', //default process.env.PGPASSWORD
    // database?: string, // default process.env.PGDATABASE || process.env.USER
    // port: 5432, // default process.env.PGPORT
    connectionString: 'postgres://postgres:password@db:5432/docker-workshop' // e.g. postgres://user:password@host:5432/database
    // ssl?: any, // passed directly to node.TLSSocket
    // types?: any, // custom type parsers
    // statement_timeout: number, // number of milliseconds before a query will time out default is no timeout
}
const client = new Client(config);
client.connect();
const sql = 'CREATE TABLE IF NOT EXISTS users(id SERIAL PRIMARY KEY, name VARCHAR(40) not null, email VARCHAR(40) not null)';
const query = client.query(sql, (err, res) => {
    client.end()
});